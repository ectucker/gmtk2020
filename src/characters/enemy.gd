extends "res://src/characters/entity.gd"


var damage = 1


func pick_move():
    if element > 0:
        combat.queue_effect(ElementalDamage.new(damage, element))
    else:
        combat.queue_effect(Damage.new(damage))
    combat.finish_move()
    combat.finish_target(combat.player)
