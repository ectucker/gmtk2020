extends Control


var health := 1
var max_health := 1

var element = -1 setget set_element

var hovered = false

var combat

var animation

var alive = true

var attack_anim

var sprite


func _ready():
    combat = find_parent("Combat")
    
    animation = find_node("AnimationPlayer", true, false)
    attack_anim = find_node("AttackAnimations", true, false)
    
    sprite = find_node("Sprite", true, false)
    
    connect("mouse_entered", self, "_mouse_entered")
    connect("mouse_exited", self, "_mouse_exited")
    
    set_element(element)

func _mouse_entered():
    if alive:
        combat.target_manager.to = self
        hovered = true

func _mouse_exited():
    if alive:
        hovered = false
    
func  _input(event):
    if hovered and event is InputEventMouseButton:
        if event.pressed and event.button_index == BUTTON_LEFT:
            combat.finish_target(self)

func damage(amount: int):
    animation.play("Hurt")
    animation.queue("Idle")
    yield(get_tree().create_timer(1.3), "timeout")
    health = clamp(health - amount, 0, max_health)
    if health == 0:
        combat.remove_enemy(self)
        modulate = Color(0, 0, 0, 0)
        hovered = false
        alive = false

func heal(amount: int):
    health = clamp(health + amount, 0, max_health)

func set_element(type):
    element = type
    find_node("Sprite").modulate = color(type)
    var healthbar = find_node("Healthbar")
    healthbar.modulate = color(type)
    if element > 0:
        find_node("Sprite").texture = sprite(type)
        healthbar.rect_position = healthbar.rect_position + Vector2.UP * 24
        if element == 2:
            healthbar.rect_position = healthbar.rect_position + Vector2.UP * 12

func color(element):
    match element:
        1:
            return Color("9E6363")
        2:
            return Color("638A9E")
        3:
            return Color("639E67")
    return Color("83A0A0")

func sprite(element):
    match element:
        1:
            return preload("res://assets/characters/enemy_fire.png")
        2:
            return preload("res://assets/characters/enemy_water.png")
        3:
            return preload("res://assets/characters/enemy_earth.png")
    return preload("res://assets/characters/enemy_pixel.png")

func play_cast():
    animation.play("Cast")
    animation.queue("Idle")
