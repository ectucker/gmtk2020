extends Level


func _init():
    spell_effects.append(Healing.new(1))
    spell_effects.append(Damage.new(1))
    spell_effects.append(Damage.new(1))
    spell_effects.append(Damage.new(1))
    
    var enemy1 = preload("res://src/enemy_control.tscn").instance()
    enemy1.max_health = 2
    enemy1.health = 2
    var enemy2 = preload("res://src/enemy_control.tscn").instance()
    enemy2.max_health = 2
    enemy2.health = 2
    var enemy3 = preload("res://src/enemy_control.tscn").instance()
    enemy3.max_health = 2
    enemy3.health = 2
    enemies.append(enemy1)
    enemies.append(enemy2)
    enemies.append(enemy3)
