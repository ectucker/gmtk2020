extends Level


func _init():
    spell_effects.append(Healing.new(3))
    spell_effects.append(Damage.new(2))
    spell_effects.append(Damage.new(2))
    spell_effects.append(Damage.new(2))
    
    var enemy1 = preload("res://src/enemy_control.tscn").instance()
    enemy1.max_health = 4
    enemy1.health = 4
    var enemy2 = preload("res://src/enemy_control.tscn").instance()
    enemy2.max_health = 4
    enemy2.health = 4
    enemies.append(enemy1)
    enemies.append(enemy2)
    
    name = "A Light from the Sky"
