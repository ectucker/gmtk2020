extends Level


func _init():
    spell_effects.append(ElementalDamage.new(1, 3))
    spell_effects.append(ElementalDamage.new(1, 2))
    spell_effects.append(ElementalDamage.new(1, 3))
    spell_effects.append(ElementalDamage.new(1, 2))
    
    var enemy1 = preload("res://src/enemy_control.tscn").instance()
    enemy1.max_health = 5
    enemy1.health = 5
    enemy1.element = 2
    var enemy2 = preload("res://src/enemy_control.tscn").instance()
    enemy2.max_health = 5
    enemy2.health = 5
    enemy2.element = 1
    enemies.append(enemy1)
    enemies.append(enemy2)
    
    name = "Red vs. Blue"
