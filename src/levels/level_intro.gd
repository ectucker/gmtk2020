extends Level


func _init():
    spell_effects.append(AoeDamage.new(1))
    spell_effects.append(Damage.new(2))
    spell_effects.append(AoeDamage.new(1))
    spell_effects.append(Damage.new(2))
    
    var enemy1 = preload("res://src/enemy_control.tscn").instance()
    enemy1.max_health = 3
    enemy1.health = 3
    enemy1.element = 1
    var enemy2 = preload("res://src/enemy_control.tscn").instance()
    enemy2.max_health = 3
    enemy2.health = 3
    enemy2.element = 2
    var enemy3 = preload("res://src/enemy_control.tscn").instance()
    enemy3.max_health = 3
    enemy3.health = 3
    enemy3.element = 3
    enemies.append(enemy1)
    enemies.append(enemy2)
    enemies.append(enemy3)

    name = "A Wide Target"
