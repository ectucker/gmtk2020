extends Level


func _init():
    spell_effects.append(Damage.new(4))
    spell_effects.append(Healing.new(4))
    spell_effects.append(Ducking.new())
    spell_effects.append(Damage.new(4))
    
    var enemy1 = preload("res://src/enemy_control.tscn").instance()
    enemy1.max_health = 5
    enemy1.health = 5
    enemy1.damage = 1
    enemy1.element = 2
    var enemy2 = preload("res://src/enemy_control.tscn").instance()
    enemy2.max_health = 5
    enemy2.health = 5
    enemy2.damage = 1
    enemy2.element = 2
    enemies.append(enemy1)
    enemies.append(enemy2)
    
    name = "Dawn of the Ducks"
