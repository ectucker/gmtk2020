extends Level


func _init():
    spell_effects.append(Healing.new(3))
    spell_effects.append(ElementalDamage.new(2, 1))
    spell_effects.append(Healing.new(3))
    spell_effects.append(ElementalDamage.new(2, 1))
    
    var enemy1 = preload("res://src/enemy_control.tscn").instance()
    enemy1.max_health = 9
    enemy1.health = 9
    enemy1.element = 3
    enemy1.damage = 2
    enemies.append(enemy1)

    name = "The Green Giant"
