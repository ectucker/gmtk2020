extends Button


var combat


func _ready():
    combat = find_parent("Combat")
    
    randomize()
    
func _pressed():
    UIHelper.play_click()
    
    if combat.turn == 0 and combat.wait_move:
        $"../AnimationPlayer".play("ReshuffleCards")
        
        var spell_parent = $"../Spells"
        
        var spells = []
        for card in spell_parent.get_children():
            card.enabled = true
            spells.append(card.effect)
        
        spells.shuffle()
        
        for i in range(0, spells.size()):
            spell_parent.get_child(i).effect = spells[i]
