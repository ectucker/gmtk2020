extends Line2D


var combat

var from: Control
var to: Control setget set_to


func _ready():
    combat = find_parent("Combat")

func show():
    if to == null:
        to = combat.player
    
    visible = true
    
    points[0] = to_local(from.rect_global_position + from.rect_size / 2)
    points[1] = to_local(to.rect_global_position + to.rect_size / 2)

func set_to(new_to):
    to = new_to
    points[1] = to_local(to.rect_global_position + to.rect_size / 2)
