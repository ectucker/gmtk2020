extends Label


var combat


func _ready():
    combat = find_parent("Combat")

func _process(delta: float):
    text = "Turn: %s" % [combat.turn]
