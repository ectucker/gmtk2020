extends Button


func _ready():
    randomize()
    
func _pressed():
    UIHelper.play_click()
    
    $"../AnimationPlayer".play("ReturnCards")
    
    var spell_parent = $"../Spells"
    
    var spells = []
    for card in spell_parent.get_children():
        card.enabled = true
        spells.append(card.effect)
    
    spells.shuffle()
    
    for i in range(0, spells.size()):
        spell_parent.get_child(i).effect = spells[i]
    
    disabled = false
