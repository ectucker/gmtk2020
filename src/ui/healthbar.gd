extends HBoxContainer


var hearts = 1 setget set_hearts

var heart_texture = preload("res://assets/ui/heart.png")
var half_heart = preload("res://assets/ui/half_heart.png")


func set_hearts(num):
    hearts = num
    var target_children = int(ceil(hearts / 2.0))
    var has_half = target_children * 2 - hearts == 1
    
    while get_child_count() < target_children:
        add_child(get_child(0).duplicate())
    while get_child_count() > target_children:
        remove_child(get_child(0))
    
    for child in get_children():
        child.texture = heart_texture
    if hearts != 0 and has_half:
        get_child(get_child_count() - 1).set_texture(half_heart)

func _process(delta):
    set_hearts($"../..".health)    
