extends Button


func _pressed():
    UIHelper.play_click()
    
    $"../../AnimationPlayer".play("ClearScreen")
    yield($"../../AnimationPlayer", "animation_finished")
    get_tree().change_scene("res://src/combat.tscn")
