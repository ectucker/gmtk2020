extends Button


func _pressed():
    UIHelper.play_click()
    
    $"../../AnimationPlayer".play("ClearScreen")
    yield($"../../AnimationPlayer", "animation_finished")
    LevelManager.index += 1
    if not LevelManager.finished():
        get_tree().change_scene("res://src/combat.tscn")
    else:
        get_tree().change_scene("res://src/ui/victory.tscn")
