extends Control


# True while waiting for input from player
var wait_move := true
var wait_target := true

# The side whose turn it is (player is 0, enemy is 1+)
var turn := 0

# The target of the current set of effects
var target = null

# Effects to resolve before giving control
var effects := []

var target_manager

var player
var enemies := []

var effect_state = 0

signal start_turn(turn)


func _ready():
    var level = LevelManager.next_level()
    for enemy in level.enemies:
        $Enemies.add_child(enemy)
    for i in range(0, 4):
        $Spells.get_child(i).effect = level.spell_effects[i]
    $LevelName.text = level.name
    
    target_manager = $TargetManager
    
    player = $Player
    for child in $Enemies.get_children():
        enemies.push_back(child)

func _process(delta: float):
    if turn < 0 or turn > enemies.size():
        turn = 0
    
    if not wait_move and not wait_target:
        if effects.size() > 0:
            if effect_state == 0:
                wait_for_effect()
            elif effect_state == 2:
                effects.remove(0)
                effect_state = 0
        else:
            turn += 1
            wait_move = true
            wait_target = true
    elif wait_move and wait_target:
        if turn > 0:
            enemies[turn - 1].pick_move()

func wait_for_effect():
    effect_state = 1
    effects.front().apply(target, get_entity(turn))
    yield(get_tree().create_timer(1.9), "timeout")
    effect_state = 2

func queue_effect(effect):
    effects.push_back(effect)

func finish_move():
    wait_move = false

func finish_target(selection):
    if wait_target:
        target = selection
        wait_target = false
        target_manager.visible = false

func get_entity(turn):
    if turn == 0:
        return player
    else:
        return enemies[turn - 1]

func remove_enemy(enemy):
    if enemy == player:
        $AnimationPlayer.play("ShowDefeat")
        set_process(false)
    
    var index = enemies.find(enemy)
    if index != -1:
        enemies.remove(index)
        if turn > index:
            turn -= 1
        if enemies.size() == 0:
            $AnimationPlayer.play("ShowVictory")

func get_neighbours(enemy):
    if enemy == player:
        return []
    
    var neighbours = []
    var index = enemies.find(enemy)
    if index > 0:
        neighbours.append(enemies[index- 1])
    if index < enemies.size() - 1:
        neighbours.append(enemies[index + 1])
    return neighbours
