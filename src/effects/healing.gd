class_name Healing


var amount: int

var description: String

func _init(healing: int):
    amount = healing
    description = "Heal target for %s" % [healing]

func apply(target, source):
    source.play_cast()
    target.heal(amount)
    target.attack_anim.play("Healing")
