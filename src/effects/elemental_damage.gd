class_name ElementalDamage


var amount: int
var element: int

var description: String

func _init(damage: int, type: int):
    amount = damage
    element = type
    description = "Deal %s %s damage to target" % [damage, type_name(type)]

func type_name(type):
    match type:
        1:
            return "fire"
        2:
            return "water"
        3:
            return "earth"
    return "normal"

func weakness(type):
    match type:
        1:
            return 2
        2:
            return 3
        3:
            return 1
    return -1

func apply(target, source):
    source.play_cast()
    if element == weakness(target.element):
        target.damage(amount * 2)
    else:
        target.damage(amount)
    match element:
        1:
            target.attack_anim.play("Fire")
        2:
            target.attack_anim.play("Water")
        3:
            target.attack_anim.play("Earth")
