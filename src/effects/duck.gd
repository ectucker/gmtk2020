class_name Ducking


var description: String

func _init():
    description = "Turn the target into a 4 HP duck"

func apply(target, source):
    source.play_cast()
    target.max_health = 4
    target.health = 4
    target.attack_anim.play("Duck")
    if target.combat.enemies.has(target):
        target.sprite.texture = preload("res://assets/characters/duck_enemy.png")
    else:
        target.sprite.texture = preload("res://assets/characters/duck_player.png")
    
