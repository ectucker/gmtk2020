class_name AoeDamage


var amount: int

var description: String

func _init(damage: int):
    amount = damage
    description = "Deal %s damage to target and it's neighbours" % [damage]

func apply(target, source):
    source.play_cast()
    var neigbours = target.combat.get_neighbours(target)
    target.damage(amount)
    target.attack_anim.play("AOE")
    for neighbour in neigbours:
        neighbour.damage(amount)
