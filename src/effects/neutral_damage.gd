class_name Damage


var amount: int

var description: String

func _init(damage: int):
    amount = damage
    description = "Deal %s damage to target" % [damage]

func apply(target, source):
    source.play_cast()
    target.damage(amount)
    target.attack_anim.play("Damage")
