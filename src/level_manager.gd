extends Node


var index = 0
var levels = []


func _ready():
    levels.append(preload("res://src/levels/level_intro.gd"))
    levels.append(preload("res://src/levels/level_healing.gd"))
    levels.append(preload("res://src/levels/level_duck.gd"))
    levels.append(preload("res://src/levels/level_elemental.gd"))
    levels.append(preload("res://src/levels/level_redblue.gd"))
    levels.append(preload("res://src/levels/level_boss.gd"))

func next_level():
    return levels[index].new()

func finished():
    return index == levels.size()
