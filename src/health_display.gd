extends Label


var entity


func _ready():
    entity = get_parent().get_parent()

func _process(delta):
    text = "HP: %s" % [entity.health]
