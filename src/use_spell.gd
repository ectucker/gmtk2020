extends Control


var combat

var effect setget set_effect

var up := false

var enabled := false setget set_enabled

func _ready():
    effect = Damage.new(1)
    
    combat = find_parent("Combat")
    connect("mouse_entered", self, "_mouse_entered")
    connect("mouse_exited", self, "_mouse_exited")

func _process(delta):
    if up and not combat.wait_move and not combat.wait_target and enabled:
        set_enabled(false)
    
func _mouse_entered():
    if enabled and combat.wait_move and combat.turn == 0:
        $AnimationPlayer.play("Hover")
        up = true

func _mouse_exited():
    if enabled and combat.wait_move and combat.turn == 0:
        $AnimationPlayer.play("Release")
        up = false

func set_effect(new_effect):
    effect = new_effect
    $Panel/Description.text = effect.description

func set_enabled(new_val):
    enabled = new_val
    $Panel/Description.visible = not enabled
    if enabled:
        $AnimationPlayer.play("Release")
        up = false
    else:
        $Play.play()

func  _gui_input(event):
    if event is InputEventMouseButton:
        if event.pressed and event.button_index == BUTTON_LEFT:
            if enabled and up and combat.turn == 0 and combat.wait_move:
                combat.queue_effect(effect)
                combat.target_manager.from = self
                combat.target_manager.show()
                combat.finish_move()
